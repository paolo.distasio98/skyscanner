//
//  FlightViewController.swift
//  Skyscanner
//
//  Created by Paolo Di Stasio on 05/12/2019.
//  Copyright © 2019 Paolo Di Stasio. All rights reserved.
//

import UIKit

struct Headline {
    
    var id : Int
    var departure : String
    var arrival : String
    var date : String
    var cost : String
    
}

class HeadlineTableViewCell : UITableViewCell {
    
    @IBOutlet weak var DepartureCity: UILabel!
    @IBOutlet weak var ArrivalCity: UILabel!
    @IBOutlet weak var Date: UILabel!
    @IBOutlet weak var Cost: UILabel!
}

var headlines : [Headline]!

class FlightViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
  var searchValues = [Flight]()
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if headlines == nil{
            tableview.alpha = 0
            return (flightData.count)
        } else {
            return headlines.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HeadlineTableViewCell

        if headlines == nil{
            let headline = flightData[indexPath.row]
            cell.DepartureCity.text = headline.departurecity
            cell.ArrivalCity.text = headline.arrivalcity
            cell.Date.text = headline.date
            cell.Cost.text = headline.cost
        } else {
            let headline = headlines[indexPath.row]
            cell.DepartureCity.text = headline.departure
            cell.ArrivalCity.text = headline.arrival
            cell.Date.text = headline.date
            cell.Cost.text = headline.cost
        }
        
        
        return(cell)
    }
    
    
    
    
    
    
    
    @IBOutlet weak var PartenzaTextField: UITextField!
    
    @IBOutlet weak var RitornoTextField: UITextField!
    
    
    private var datePicker: UIDatePicker?
    
    private var datePickerRitorno : UIDatePicker?
    
    @IBOutlet weak var tableview: UITableView!
    
    
    @IBOutlet weak var FromTextField: UITextField!
    
    @IBOutlet weak var ToTextField: UITextField!
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        let startdata = DateFormatter()
        startdata.dateFormat = "dd/MM/yyyy"
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePicker?.addTarget(self, action: #selector(FlightViewController.dateChangedPartenza(datePicker:)), for: .valueChanged)
        
        datePickerRitorno = UIDatePicker()
        datePickerRitorno?.datePickerMode = .date
        datePickerRitorno?.addTarget(self, action: #selector(FlightViewController.dateChangedRitorno(datePickerRitorno:)), for: .valueChanged)
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gestureRecognizer:)))
        
        
        
        RitornoTextField.inputView = datePickerRitorno
        PartenzaTextField.inputView = datePicker
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped(gestureRecognizer : UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    @objc func dateChangedPartenza(datePicker: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        PartenzaTextField.text = dateFormatter.string(from: datePicker.date)
    }
    
    @objc func dateChangedRitorno(datePickerRitorno : UIDatePicker){
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "dd/MM/yyyy"
        RitornoTextField.text = dateFormatter1.string(from: datePickerRitorno.date)
    }
    
    
    @IBAction func ChangeCityAction(_ sender: Any) {
        var string1 : String
        var string2 : String
        
        string1 = FromTextField.text!
        string2 = ToTextField.text!
        
        FromTextField.text = string2
        ToTextField.text = string1
       

    }
    
    @IBAction func searchbuttonaction(_ sender: Any) {
        
        if FromTextField.text!.isEmpty || ToTextField.text!.isEmpty || PartenzaTextField.text!.isEmpty {
            let alert = UIAlertController(title: "Alert", message: "Fill all the fields", preferredStyle: .alert)
                               alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil ))
                               self.present(alert, animated: true, completion: nil)
            
        }
        else {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let dateDepart = formatter.date(from:PartenzaTextField!.text!)!
        headlines = nil


        let numberflight = flightData.count

        for i in 0...numberflight-1 {
            let date = formatter.date(from: flightData[i].date)!
           
            
            if RitornoTextField.text!.isEmpty {
                if FromTextField.text?.lowercased() == flightData[i].departurecity.lowercased() && ToTextField.text?.lowercased() == flightData[i].arrivalcity.lowercased() && dateDepart < date {
                    
                    if headlines == nil{
                        headlines = [Headline(id: i, departure: flightData[i].departurecity, arrival: flightData[i].arrivalcity, date:flightData[i].date, cost: flightData[i].cost)]
                    } else {
                        headlines?.append(Headline(id: i, departure: flightData[i].departurecity, arrival: flightData[i].arrivalcity, date:flightData[i].date, cost: flightData[i].cost))
                    }
                    tableview.alpha = 1

                }
            }
            else {
                let dateReturn = formatter.date(from: RitornoTextField!.text!)!
                if dateDepart<dateReturn {
                    if FromTextField.text?.lowercased() == flightData[i].departurecity.lowercased() && ToTextField.text?.lowercased() == flightData[i].arrivalcity.lowercased() && dateDepart < date {
                                      
                                      if headlines == nil{
                                          headlines = [Headline(id: i, departure: flightData[i].departurecity, arrival: flightData[i].arrivalcity, date:flightData[i].date, cost: flightData[i].cost)]
                                      } else {
                                          headlines?.append(Headline(id: i, departure: flightData[i].departurecity, arrival: flightData[i].arrivalcity, date:flightData[i].date, cost: flightData[i].cost))
                                      }
                                      tableview.alpha = 1
                                  }
                                  
                                  if ToTextField.text?.lowercased() == flightData[i].departurecity.lowercased() && FromTextField.text?.lowercased() == flightData[i].arrivalcity.lowercased() && dateReturn < date {
                                      
                                      if headlines == nil{
                                          headlines = [Headline(id: i, departure: flightData[i].departurecity, arrival: flightData[i].arrivalcity, date:flightData[i].date, cost: flightData[i].cost)]
                                      } else {
                                          headlines?.append(Headline(id: i, departure: flightData[i].departurecity, arrival: flightData[i].arrivalcity, date:flightData[i].date, cost: flightData[i].cost))
                                      }
                                      tableview.alpha = 1
                                  }
                }
                else {
                    let alert = UIAlertController(title: "Alert", message: "Wrong date", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil ))
                    self.present(alert, animated: true, completion: nil)
                }

              
                
            }
                
            tableview.reloadData()

            }
        
            

        }
        

    }
}
    
    


