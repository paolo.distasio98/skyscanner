//
//  data.swift
//  Skyscanner
//
//  Created by Paolo Di Stasio on 09/12/2019.
//  Copyright © 2019 Paolo Di Stasio. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI
import CoreLocation

let flightData: [Flight] = load(filename: "flight.json")

func load<T: Decodable>( filename: String) -> T {
    let data : Data
    
    guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
        else {
            fatalError("Couldn't find \(filename) in main bundle.")
    }
    
    do {
        data = try Data(contentsOf: file)
    } catch {
        fatalError("Couldn't load \(filename) from main bundle:\n\(error)")

    }
    
    do{
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    }
    catch {
         fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")
    }
}
