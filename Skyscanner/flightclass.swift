//
//  flightclass.swift
//  Skyscanner
//
//  Created by Paolo Di Stasio on 09/12/2019.
//  Copyright © 2019 Paolo Di Stasio. All rights reserved.
//

import Foundation
import CoreLocation

struct Flight: Hashable, Codable{
    var departurecity: String
    var arrivalcity: String
    var date: String
    var cost: String
}
